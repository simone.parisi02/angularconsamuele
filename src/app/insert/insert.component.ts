import { Component, OnInit } from '@angular/core';
import {AttoreService} from '../service/attoreservice';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  constructor(public attoriService: AttoreService) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.attoriService.addAttore(this.attoriService.form.value).subscribe(result => console.log(result));
  }

}
