import { Component, OnInit } from '@angular/core';
import { Attore } from '../entity/Attore';
import { AttoreService } from '../service/attoreservice';

@Component({
  selector: 'app-modifica',
  templateUrl: './modifica.component.html',
  styleUrls: ['./modifica.component.css']
})
export class ModificaComponent implements OnInit {

  attore:Attore | undefined;
  
  constructor(private attoreService: AttoreService) { }

  ngOnInit(): void {
  }

  getInfoAttore(codAttore: Number){
    this.attoreService.getAttoreByID(codAttore).subscribe((result:Attore)=>{
      this.attore = new Attore(result.codAttore, result.nome, result.annoNascita, result.country);
      this.attoreService.initializeFormGroup(this.attore);
    })

  }

}
