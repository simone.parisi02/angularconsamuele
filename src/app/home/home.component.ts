import { Component, OnInit } from '@angular/core';
import {AttoreService} from '../service/attoreservice';
import {Router} from '@angular/router';
import {ModificaComponent} from '../modifica/modifica.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(router: Router, attoreService: AttoreService) {
    this.attoreService = attoreService;
    this.router = router;
  }

  result: any;
  key:any;
  router:Router;
  attoreService:AttoreService;

  ngOnInit(): void {
  }

  visualizza(){
    this.attoreService.getAttori().subscribe((attori:any)=>{
      this.result = attori;
      this.key = Object.keys(attori[0]);
      
    })
  }

  performDelete(codAttore:Number){
    this.attoreService.deleteAttore(codAttore);
  }

  performUpdate(codAttore:Number){
    this.router.navigate(['/putAttore']).then(() = {
      updateAttoreComponent.prototype.getInfoAttore(codAttore);
    });
  }
}

