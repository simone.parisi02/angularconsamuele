import { Injectable} from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { Attore } from '../entity/Attore';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Injectable({
    providedIn:'root'
})
export class AttoreService{
    serverAddresse = 'http://localhost:8080/api/attori';
    constructor(private http:HttpClient){}

    private option = {headers:new HttpHeaders().set('Access-Control-Allow-Origin', '*')}
    
    getAttori(){
        return this.http.get(this.serverAddresse, this.option);
    }

    public form: FormGroup = new FormGroup({
        nome: new FormControl('', Validators.required),
        annoNascita: new FormControl(1990),
        country: new FormControl('Italy')
    });

    initializeFormGroup(attore:Attore){
        this.form.setValue({
            nome:attore.nome,
            annoNascita:attore.annoNascita,
            country:attore.country
        });
    }

    addAttore(attore: Attore){
        return this.http.post(this.serverAddresse, attore);
    }

    deleteAttore(codAttore: Number){
        return this.http.delete(this.serverAddresse+"/"+codAttore);
    }

    getAttoreByID(codAttore: Number){
        return this.http.get(this.serverAddresse+"/"+codAttore, this.option);
    }

    putAttore(attore:Attore){
        return this.http.put(this.serverAddresse,attore);
    }
}
