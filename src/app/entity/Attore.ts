export class Attore{
    codAttore:Number;
    nome:String;
    annoNascita: Number;
    country: String;

    constructor(codAttore:Number, nome:String, annoNascita:Number, country:String){
        this.codAttore = codAttore;
        this.annoNascita = annoNascita;
        this.country = country;
        this.nome = nome;
    }

}